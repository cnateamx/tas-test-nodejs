'use strict';

const express = require('express');
const redis = require('redis');
const amqp = require('amqplib/callback_api');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  var mysql = require('mysql');
  
  var con = mysql.createConnection({
    host:"2da2a566-ebd4-4f77-846e-5e27add9b5f8.mysql.service.internal",
    user:"ff2612bca9f842a9825c3bbeb89c170c",
    password:"u7u8hyuz7w4sz5ne",
    database:"service_instance_db"
  });
  
  // mySQL test
  con.connect(function(err) {
    if (err) throw err;
    con.query("SELECT firstname, lastname FROM my_users", function (err, result, fields) {
      if (err) throw err;
      console.log(result);
      var string=JSON.stringify(result);
      console.log(string);
      var json =  JSON.parse(string);

      res.send('Hello ' + json[0].firstname + ' ' + json[0].lastname + '. Results are retrieved from mySQL.... use /redis to test redis cache');
    });
  });
});

//redis test
app.get('/redis', async(req, res) => {
  var redisKey = 'greetings';
  var client = redis.createClient({
    url: 'redis://q-s0.redis-instance.services.service-instance-3b58bc50-5c52-4b44-9692-472ef6e4bbd0.bosh:6379',
    password: 'E0bo4M5nSm39wBEMtPLcBkVdUiepAh'
  });

  await client.connect();
  await client.set(redisKey, 'Hello, this is from redis!!');
  var redisResult = await client.get(redisKey);
  
  if (redisResult == null) {
    res.send('no results');
  }else{
    res.send(redisResult);
  }
  await client.disconnect();
});

//rabbitMQ Producer test
app.get('/rabbitMQProducer', (req, res) => {
  amqp.connect('amqp://c2790e6c-49aa-4dcd-8f41-00d781e18dbe:aEaG_c7NveSIRN6vPc7l8FMM@q-s0.rabbitmq-server.services.service-instance-6bf41461-2cac-4021-8608-a395f8c6c523.bosh/6bf41461-2cac-4021-8608-a395f8c6c523', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'hello';
        var msg = 'Hello, this message is sent from nodeJS producer';

        channel.assertQueue(queue, {
            durable: false
        });
        channel.sendToQueue(queue, Buffer.from(msg));
        res.send("Message has been sent!");
        console.log(" [x] Sent %s", msg);
    });
    setTimeout(function() {
        connection.close();
    }, 500);
  });
});

app.get('/rabbitMQConsumer', (req, res) => {
  amqp.connect('amqp://c2790e6c-49aa-4dcd-8f41-00d781e18dbe:aEaG_c7NveSIRN6vPc7l8FMM@q-s0.rabbitmq-server.services.service-instance-6bf41461-2cac-4021-8608-a395f8c6c523.bosh/6bf41461-2cac-4021-8608-a395f8c6c523', function(error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function(error1, channel) {
      if (error1) {
          throw error1;
      }

      var queue = 'hello';

      channel.assertQueue(queue, {
          durable: false
      });

      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

      channel.consume(queue, function(msg) {
          console.log(" [x] Received %s", msg.content.toString());
      }, {
          noAck: true
      });
    });
    setTimeout(function() {
      connection.close();
      res.send("Connection Closed");
    }, 500);
  });
});


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);